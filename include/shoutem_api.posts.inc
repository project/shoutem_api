<?php

define('SHOUTEM_API_POSTS_LIMIT', 100);

define('SHOUTEM_API_COMMENTS_LIMIT', 100);

define('SHOUTEM_API_CATEGORIES_ALL', 0);

/**
 * Set default values for arguments
 *
 * @param array $args
 *   Array of input values
 * @return array
 *   Array of arguments
 */
function _helper_posts_find_args($args) {

    if (isset($args['session_id'])) {
        // Load user session from session_id argument
        _shoutem_api_session_load($args);
    }

    return array(
        'category_id' => isset($args['category_id']) ? (int)$args['category_id'] : NULL,
        'offset' => isset($args['offset']) ? (int)$args['offset'] : 0,
        'limit' => (isset($args['limit']) && $args['limit'] <= SHOUTEM_API_POSTS_LIMIT) ? (int)$args['limit'] : SHOUTEM_API_POSTS_LIMIT,
    );
}

function _helper_get_node_types() {
    $node_types = array();
    
    $var_node_types = variable_get('shoutem_api_node_types', NULL);
    foreach ($var_node_types as $type => $value) {
        if ($value) {
            $node_types[] = $type;
        }
    }

    return $node_types;
}

/**
 * Finds all nodes that match selected conditions.
 *
 * @return
 *   A resource identifier pointing to the query results.
 */
function _helper_posts_find($args) {

    global $_shoutem_num_records;

    $sql_join = '';
    $sql_where = array();
    $sql_args = array();
    
    $args = _helper_posts_find_args($args);

    // Check if user has access to content
    if (!user_access('access content')) {
        _shoutem_api_error_response(403);
    }

    if (
        module_exists('taxonomy') && // If taxonomy (category) module is enabled
        !is_null($args['category_id']) && // and category argument is provided
        ($args['category_id'] != SHOUTEM_API_CATEGORIES_ALL) // and category is not all
    ) {
        $term = taxonomy_get_term($args['category_id']);
        $tree = taxonomy_get_tree($term->vid, $term->tid, -1, NULL); // Traverse entire tree
        $tids = array_merge(array($term->tid), array_map('_taxonomy_get_tid_from_term', $tree));

        $sql_join = 'INNER JOIN {term_node} tn ON n.vid = tn.vid';
        $sql_where[] = 'tn.tid IN (' . db_placeholders($tids, 'int') . ') AND';
        $sql_args = array_merge($sql_args, $tids);
    }

    // Handle node types
    $node_types = _helper_get_node_types();

    $sql_where[] = 'n.type IN (' . db_placeholders($node_types, 'varchar') .') AND';
    $sql_args = array_merge($sql_args, $node_types);

    // Build queries
    $sql_query = 'SELECT DISTINCT(n.nid) FROM {node} n ' . $sql_join . '
            WHERE ' . implode(' ', $sql_where) . ' n.status = 1
            ORDER BY n.sticky DESC, n.created DESC';

    $sql_query_c = 'SELECT COUNT(DISTINCT(n.nid)) FROM {node} n ' . $sql_join . '
            WHERE ' . implode(' ', $sql_where) . ' n.status = 1';

    $_shoutem_num_records = db_result(db_query(db_rewrite_sql($sql_query_c), $sql_args));

    return db_query_range(db_rewrite_sql($sql_query), $sql_args, $args['offset'], $args['limit']);
}

function _helper_get_paging($args) {

    global $_shoutem_num_records;

    $result = array();

    // If we have no records return no paging data (empty array)
    if ($_shoutem_num_records < 1) {
        return $result;
    }

    $uri = preg_replace(array('/&offset=[0-9]+/', '/&limit=[0-9]+/'), '', request_uri());
    $uri .= '&offset=%d&limit=%d';

    $limit = (isset($args['limit']) && $args['limit'] <= SHOUTEM_API_POSTS_LIMIT) ? (int)$args['limit'] : SHOUTEM_API_POSTS_LIMIT;

    $pages = ceil($_shoutem_num_records / $limit);

    $current_page = ceil($args['offset'] / $limit) + 1;

    if ($current_page > 1) {
        $offset = ($current_page - 2) * $limit;
        $result['paging']['previous'] = sprintf($uri, $offset, $limit);
    }

    if ($current_page < $pages) {
        $offset = $current_page * $limit;
        $result['paging']['next'] =  sprintf($uri, $offset, $limit);
    }

    return $result;
}

function _helper_load_node($nid, $full = FALSE) {

    $node = node_load(array('nid' => $nid));

    if (!$node) {
        return FALSE;
    }
        
    if (!in_array($node->type, _helper_get_node_types())) {
        _shoutem_api_error_response(403);
    }

    $post = array();
    $post['post_id'] = $node->nid;
    $post['category_ids'] = array_keys($node->taxonomy);
    $post['published_at'] = format_date($node->created, 'custom', SHOUTEM_API_DATETIME_FORMAT);
    $post['author'] = $node->name;
    $post['title'] = $node->title;
    $post['summary'] = $node->teaser;

    if ($full) {
        $post['body'] = $node->body;
    }

    if ($node->comment == COMMENT_NODE_READ_WRITE) {
        $post['commentable'] = user_access('post comments') ? "yes" : "denied";
    }
    else {
        $post['commentable'] = "no";
    }

    $post['comments_count'] = (int)$node->comment_count;

    $post['likeable'] = FALSE; // Not supported
    //$post['likes_count'] = 0; // Not supported

    $post['link'] = url('node/' . $node->nid, array('absolute' => TRUE));

    return $post;
}

/**
 * Process and validate input arguments. Also set defaults if not specified.
 *
 * @param array $args
 *   Input arguments
 * @return array
 *   Processed arguments
 */
function _helper_posts_comments_args($args) {
    return array(
        'post_id' => (isset($args['post_id']) && $args['post_id'] > 0) ? (int)$args['post_id'] : NULL,
        'session_id' => isset($args['session_id']) ? $args['session_id'] : NULL,
        'offset' => isset($args['offset']) ? (int)$args['offset'] : 0,
        'limit' => (isset($args['limit']) && $args['limit'] <= SHOUTEM_API_COMMENTS_LIMIT) ? (int)$args['limit'] : SHOUTEM_API_COMMENTS_LIMIT,
    );
}

/**
 * Map db result item to ShoutEm comment data structure
 *
 * @param array $data
 *   Array of values
 * @return array
 *   ShoutEM comment data structure
 */
function _helper_comment_remap($data) {

    $comment = array();
    $comment['comment_id'] = $data['cid'];
    $comment['published_at'] = format_date($data['timestamp'], 'custom', SHOUTEM_API_DATETIME_FORMAT);
    $comment['author'] = $data['name'];

    $comment['message'] = $data['comment'];

    $comment['likeable'] = FALSE; // Not supported
    //$post['likes_count'] = 0; // Not supported

    $comment['deletable'] = user_access('administer comments') ? TRUE : FALSE;

    return $comment;
}

/**
 * Load user picture if visitor has active permission "access user profiles"
 *
 * @see template_preprocess_user_picture()
 * @param mixed $uid
 *   User ID
 * @return array
 */
function _helper_get_user_picture($uid) {

    $result = array();

    if (variable_get('user_pictures', 0)) {

        $account = user_load(array('uid' => $uid));

        if (!empty($account->picture) && file_exists($account->picture)) {
            $picture = file_create_url($account->picture);
        }
        elseif (variable_get('user_picture_default', '')) {
            $picture = variable_get('user_picture_default', '');
        }

        if (isset($picture)) {
            if (!empty($account->uid) && user_access('access user profiles')) {
                $result['author_image_url'] = $picture;
            }
        }
  }

  return $result;
}

/**
 *
 * @param string $form
 *   Form name
 * @param array $form_state
 *   Form values
 * @return integer
 *   Comment ID if saved successfully, FALSE otherwise
 */
function _helper_posts_comments_save($form, &$form_state) {

    _comment_form_submit($form_state['values']);

    if ($cid = comment_save($form_state['values'])) {
        $node = node_load($form_state['values']['nid']);
        // Add 1 to existing $node->comment count to include new comment being added.
        $comment_count = $node->comment_count + 1;
        return $cid;
    }
    else {
        return FALSE;
    }
}

/**
 * Map Drupal comment object to ShoutEm response structure
 *
 * @param integer $cid
 *   Comment ID
 * @return array
 *   ShoutEm Comment submit response structure
 */
function _helper_posts_comments_map_comment($cid) {

    $data = _comment_load($cid);

    $result = array();

    if (!$data) {
        return $result;
    }

    $result['comment_id'] = (int)$data->cid;
    $result['parent_comment_id'] = (int)$data->pid;
    $result['published_at'] = format_date($data->timestamp, 'custom', SHOUTEM_API_DATETIME_FORMAT);
    $result['author'] = $data->name ? $data->name : variable_get('anonymous', t('Anonymous'));
    $result['message'] = $data->comment;
    $result['likeable'] = FALSE;
    $result['approved'] = ($data->status == COMMENT_PUBLISHED) ? TRUE : FALSE;

    return $result;
}

/*********** API Functions below ************/

function _shoutem_api_posts_categories() {

    // Return empty array if categorization is not enabled
    if (!module_exists('taxonomy')) {
        return array();
    }

    $vid = variable_get('shoutem_api_category_vid', NULL);
    $terms = taxonomy_get_tree($vid); // TODO: In v2 add tree depth configuration option?

    $categories = array(
        array(
            'category_id'   => SHOUTEM_API_CATEGORIES_ALL,
            'name'          => check_plain(variable_get('site_name', 'All')),
            'allowed'       => true
        )
    );

    foreach ($terms as $term) {
        $categories[] = array(
            'category_id'   => $term->tid,
            'name'          => check_plain($term->name),
            'allowed'       => true
        );
    }

    return array('data' => $categories);
}

function _shoutem_api_posts_find($args) {

    $result = array('data' => array());

    $post_ids = _helper_posts_find($args);

    while ($nid = db_result($post_ids)) {
        $result['data'][] = _helper_load_node($nid);
    }

    $result = array_merge($result, _helper_get_paging($args));

    return $result;
}

function _shoutem_api_posts_get($args) {

    $nid = (int)$args['post_id'];

    $post = _helper_load_node($nid, TRUE); // TRUE arg -> Load full node

    $node = node_load(array('nid' => $nid));

    // Determine whether the current user may perform view operation on the specified node
    if (!node_access('view', $node)) {
        _shoutem_api_error_response(403);
    }

    if (!$post) {
        _shoutem_api_error_response(404);
    }

    return $post;
}

function _shoutem_api_posts_comments($args) {

    // Check if module is enabled
    if (!module_exists('comment')) {
        _shoutem_api_error_response(501);
    }

    $args = _helper_posts_comments_args($args);

    // Load node and validate post_id argument
    $node = node_load($args['post_id']);

    if (FALSE == $node) {
        _shoutem_api_error_response(404);
    }

    // If post type is not within allowed types
    if (!in_array($node->type, _helper_get_node_types())) {
        _shoutem_api_error_response(403);
    }

    // If comments are disabled return 403
    if (!$node->comment) {
        _shoutem_api_error_response(403);
    }

    // Load user session from session_id argument
    if (!is_null($args['session_id'])) {
        _shoutem_api_session_load($args);
    }

    // Check if user has access to the comments
    if (!user_access('access comments')) {
        _shoutem_api_error_response(403);
    }

    // Set number of comments
    $GLOBALS['_shoutem_num_records'] = $node->comment_count;

    $result = array('data' => array());

    if ($GLOBALS['_shoutem_num_records'] > 0) {
        $qr = db_query_range("SELECT * FROM {comments} WHERE nid = %d AND status = %d ORDER BY thread DESC", $args['post_id'], COMMENT_PUBLISHED, $args['offset'], $args['limit']);
        while ($comment = db_fetch_array($qr)) {
            // Map result item to shoutem comment structure
            $comment_data = _helper_comment_remap($comment);
            // Add user avatar/picture
            $comment_data = array_merge($comment_data, _helper_get_user_picture($comment['uid']));
            $result['data'][] = $comment_data;
        }
    }

    // Merge paging to result set
    $result = array_merge($result, _helper_get_paging($args));

    return $result;
}

/**
 * Adds a new comment to a node and returns the comment.
 *
 * @param $args
 *   Array of arguments
 * @return
 *   ShoutEm comment data structure
 */
function _shoutem_api_posts_comments_new($args) {

    global $user;
    $comment_fields = array();

    // Check if module is enabled
    if (!module_exists('comment')) {
        _shoutem_api_error_response(501);
    }

    // Load user session from session_id argument if specified
    if (isset($args['session_id'])) {
        _shoutem_api_session_load($args);
    }

    // Load node and validate post_id argument
    $node = node_load($args['post_id']);
    if (FALSE == $node) {
        _shoutem_api_error_response(404);
    }

    // If post type is not within allowed types
    if (!in_array($node->type, _helper_get_node_types())) {
        _shoutem_api_error_response(403);
    }

    // Check whether comments are enabled for this particular node or node's type
    // or user doesn't have permission to post comments
    if ($node->comment != COMMENT_NODE_READ_WRITE ||
        (variable_get('comment_'. $node->type, COMMENT_NODE_DISABLED) != COMMENT_NODE_READ_WRITE) ||
        !user_access('post comments')) {
        _shoutem_api_error_response(403);
    }

    // Check validity of author name and email
    if (!$user->uid) {
        if (variable_get('comment_anonymous_' . $node->type, COMMENT_ANONYMOUS_MAYNOT_CONTACT) > COMMENT_ANONYMOUS_MAYNOT_CONTACT) {
            if ($args['author_nickname']) {
                $taken = db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE LOWER(name) = '%s'", $args['author_nickname']));

                if ($taken != 0) {
                    // Invalid param: The name you used belongs to a registered user
                    _shoutem_api_error_response(400);
                }
                else {
                    $comment_fields['values']['author'] = $args['author_nickname'];
                }
            }
            elseif (variable_get('comment_anonymous_' . $node->type, COMMENT_ANONYMOUS_MAYNOT_CONTACT) == COMMENT_ANONYMOUS_MUST_CONTACT) {
                // Invalid param: You have to leave your name
                _shoutem_api_error_response(400);
            }

            if ($args['author_email']) {
                if (valid_email_address($args['author_email'])) {
                    $comment_fields['values']['mail'] = $args['author_email'];
                }
                else {
                    // Invalid param: The e-mail address you specified is not valid
                    _shoutem_api_error_response(400);
                }
            }
            elseif (variable_get('comment_anonymous_' . $node->type, COMMENT_ANONYMOUS_MAYNOT_CONTACT) == COMMENT_ANONYMOUS_MUST_CONTACT) {
                 // Invalid param: You have to leave an e-mail address
                _shoutem_api_error_response(40);
            }
        }
        else {
            // Anonymous posters may not enter their contact information so we won't process input args
        }

    }
    else {
        // Get author name from user
        $comment_fields['values']['author'] = $user->name;
    }

    $comment_fields['values']['nid'] = $node->nid;
    $comment_fields['values']['comment'] = $args['message'];
    if (isset($args['parent_coment_id']) && is_numeric($args['parent_coment_id'])) {
        $comment_fields['values']['pid'] = $args['parent_coment_id'];
    }
    $comment_fields['values']['op'] = t('Save');

    $cid = _helper_posts_comments_save("comment_form", $comment_fields);

    return _helper_posts_comments_map_comment($cid);
}

function _shoutem_api_posts_comments_delete($args) {

    // Check if module is enabled
    if (!module_exists('comment')) {
        _shoutem_api_error_response(501);
    }

    // Load user session from session_id argument if specified
    _shoutem_api_session_load($args);

    // Check if user can delete comment
    if (!user_access('administer comments')) {
        _shoutem_api_error_response(403);
    }

    // Load node and validate post_id argument
    $comment = _comment_load($args['comment_id']);
    if (FALSE == $comment) {
        _shoutem_api_error_response(404);
    }

    module_load_include('inc', 'comment', 'comment.admin');

    // Delete comment and its replies.
    _comment_delete_thread($comment);

    // Update comment count
    _comment_update_node_statistics($comment->nid);

    // Clear the cache so an anonymous user sees that his comment was deleted.
    cache_clear_all();

    return array();
}