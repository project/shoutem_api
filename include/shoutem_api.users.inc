<?php

function _shoutem_api_users_authenticate($args) {
  global $user;

  $user = user_authenticate(array('name' => $args['username'], 'pass' => $args['password']));

  if ($user->uid) { // TODO: In v2.0 - Check user_access('access using shoutem api', $user)
    // Regenerate the session ID to prevent against session fixation attacks.
    sess_regenerate();
    $array = array();
    user_module_invoke('login', $array, $user);

    return array('session_id' => session_id());
  }
  else {
      _shoutem_api_error_response(401);
  }

}

function _shoutem_api_users_logout($args) {

    global $user;

    _shoutem_api_session_load($args);

    module_invoke_all('user_logout', $user);

    // Destroy the current session, and reset $user to the anonymous user.
    session_destroy();
}