<?php

function _shoutem_api_events_calendars() {

    $result = array();

    if (module_exists('event')) {
        $result[] = url('event/ical', array('absolute' => TRUE));
    }

    if (module_exists('calendar_ical')) {

        $views = views_get_all_views();

        foreach ($views as $view) {
            // Skip disabled or broken views.
            if (!empty($view->disabled) || empty($view->display)) {
                continue;
            }

            foreach (array_keys($view->display) as $id) {
                if ($view->display[$id]->display_plugin == "calendar_ical") {
                    $uri = $view->display[$id]->display_options['path'];
                    if ($uri) {
                        $result[] = url($uri, array('absolute' => TRUE));
                    }
                }
            }
        }
    } // module_exists('calendar_ical')

    return $result;
}