<?php

function _shoutem_api_service_info() {

    $result = array(
        'api_version' => SHOUTEM_API_VERSION,
        'server_type' => 'drupal'
    );

    return $result;
}
