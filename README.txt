About ShoutEm
-------------

ShoutEm is a web platform that allows you to turn your website into a full-featured mobile app in minutes. Add your existing RSS feeds, YouTube videos, podcasts and more to have all your content available in a single app! Structure your feeds into categories, promote certain categories on your home screen, and build a full featured browsing app for your users. Users will be able to comment, like and share your content to Facebook, Twitter and Foursquare with links that lead back to your website and help build traffic.

ShoutEm API Drupal module allows you to bring your Drupal-based site to the growing population of users who read content from mobile devices like iPhones, iPods, iPads or Android based phones.

ShoutEm API module serves as a connector between your Drupal web site and the mobile application you build on ShoutEm. It exposes a REST API that allows your users to read blog posts from your site and comment them directly from your mobile application.

Customization options allow you, the Drupal site owner, to choose content types that will be exposed through the API.

Recommended modules
-------------------

Comment and Taxonomy (Drupal core modules)
Calendar, Events - for making your events available to ShoutEm platform

Before reporting bugs please make sure you are running the latest version. Bugs in the ShoutEm API module can also be reported at http://www.shoutem.com/support

Installation instructions
-------------------------

Download module to your local hard drive and then upload the archive in the modules folder of your Drupal installation and extract it. The best practice is to keep all of your contributed modules in the sites/all/modules.

After extracting the module, you can enable it from your administration area. Navigate to Administer > Site building > Modules. Check the 'Enabled' box next to the Shoutem API module (Located in category "Other") and then click the 'Save Configuration' button at the bottom.

Also we are suggesting you to enable Taxonomy and Comment modules. Both are optional core modules and ShoutEm API is using them for content categorization and to allow mobile clients to discuss published content.

After you have successfully enabled ShoutEm API module, you need to configure it. Navigate to Administer > Site configuration > ShoutEm API.

There you can specify which content types can be accessed from mobile clients.

If you have defined multiple taxonomy vocabularies you can specify one that is best describing your content. It will be used for representing categories in our mobile clients.

ShoutEm Terms of Use
--------------------

The ShoutEm API Drupal module is licensed under the same license as Drupal core and is free. However, mobile applications using ShoutEm platform are not free. Check out ShoutEm web site (www.shoutem.com) for the latest pricing info.

Last update
-----------
