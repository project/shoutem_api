<?php

/**
 * Get list of vocabularies (categories) together with node types
 *
 * @param array $node_types Array of strings with node types
 * @return string
 */
function _helper_get_vocabulary_list($node_types) {

  $vocabularies = taxonomy_get_vocabularies();
  $result = array();

  foreach ($vocabularies as $vocabulary) {
    $types = array();
    foreach ($vocabulary->nodes as $type) {
      $types[] = $node_types[$type];
    }

    $types_string = implode(', ', $types);
    $result[$vocabulary->vid] = check_plain($vocabulary->name);
    $result[$vocabulary->vid] .= (drupal_strlen($types_string)) ? ' (' . check_plain($types_string) . ')' : '';
  }

  return $result;
}

/**
 * Setup module config admin form
 */
function shoutem_api_admin_settings() {
  // List of available content types
  $node_types = array_map('check_plain', node_get_types('names'));

  $defaults = isset($node_types['blog']) ? array('blog' => 1) : array();

  $form['shoutem_api_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable for external ShoutEm clients'),
    '#required' => TRUE,
    '#default_value' => variable_get('shoutem_api_node_types', $defaults),
    '#options' => $node_types,
    '#description' => t('Select the content types available to ShoutEm clients via REST.')
  );

  // Check if Taxonomy module is defined
  if (module_exists('taxonomy')) {
      $vocabs = _helper_get_vocabulary_list($node_types);
      $description = 'Selected vocabulary that best describes your content.';
      $description_no_cat = ' Currently, there are no categories defined. Please go to the <a href="@url">taxonomy administration</a> page and create at least one vocabulary.';

      $form['shoutem_api_category_vid'] = array(
        '#type' => 'radios',
        '#title' => t('Choose content category'),
        '#required' => empty($vocabs) ? FALSE : TRUE,
        '#default_value' => variable_get('shoutem_api_category_vid', NULL),
        '#options' => $vocabs,
        '#description' => empty($vocabs) ? t($description_no_cat, array('@url' => url('admin/content/taxonomy'))) : t($description),
      );
  }
  else {
      $form['shoutem_api_category_vid'] = array(
          '#title' => t('Category settings'),
          '#prefix' => '<div>',
          '#value' => '<em>' . t('Note: You don\'t have the Taxonomy module enabled.') . '</em>',
          '#suffix' => '</div>'
      );
  }

  return system_settings_form($form);
}